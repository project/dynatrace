<?php

namespace Drupal\Tests\dynatrace\Unit;

use Drupal\dynatrace\Api\Count;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\dynatrace\Api\Count
 *
 * @group dynatrace
 */
class CountTest extends UnitTestCase {

  public function testToString(): void {
    $count = new Count('example', ['dimension' => 'value']);
    $this->assertSame('example,dimension="value" count,delta=1', (string) $count);
    $count->setDelta(200);
    $this->assertSame('example,dimension="value" count,delta=200', (string) $count);
  }

}
