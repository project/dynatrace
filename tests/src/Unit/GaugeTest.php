<?php

namespace Drupal\Tests\vgwort\Unit;

use Drupal\dynatrace\Api\Gauge;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\dynatrace\Api\Gauge
 *
 * @group dynatrace
 */
class GaugeTest extends UnitTestCase {

  public function testToString(): void {
    $gauge = new Gauge('test', ['type' => 'ABC', 'label' => '4321']);
    $this->assertSame('test,type="ABC",label="4321" gauge,1', (string) $gauge);
    $gauge->addValue(2);
    $this->assertSame('test,type="ABC",label="4321" gauge,2', (string) $gauge);
    $gauge->addValue(6);
    $gauge->addValue(3);
    $this->assertSame('test,type="ABC",label="4321" gauge,min=2,max=6,sum=11,count=3', (string) $gauge);
    $gauge->addValue(10.5);
    $this->assertSame('test,type="ABC",label="4321" gauge,min=2,max=10.5,sum=21.5,count=4', (string) $gauge);

    $gauge = new Gauge('test');
    $gauge->addValue(6);
    $gauge->addValue(3);
    $this->assertSame('test gauge,min=3,max=6,sum=9,count=2', (string) $gauge);
  }

}
