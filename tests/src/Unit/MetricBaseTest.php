<?php

namespace Drupal\Tests\dynatrace\Unit;

use Drupal\dynatrace\Api\MetricBase;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\dynatrace\Api\MetricBase
 *
 * @group dynatrace
 */
class MetricBaseTest extends UnitTestCase {

  /**
   * @dataProvider providerToString
   */
  public function testToString(string $expected, array $args, ?string $host = NULL): void {
    $metric = new Metric(...$args);
    if (is_string($host)) {
      $metric->setHost($host);
    }
    $this->assertSame($expected, (string) $metric);
  }

  public static function providerToString() {
    return [
      [
        'test,type="ABC",label="4321" 1',
        ['test', ['type' => 'ABC', 'label' => '4321']],
      ],
      [
        'test2 1 1668634291858',
        ['test2', [], 1668634291858],
      ],
      [
        'test,dt.entity.host="HOST-2345EFA5657",type="ABC",label="4321" 1',
        ['test', ['type' => 'ABC', 'label' => '4321']],
        'HOST-2345EFA5657',
      ],
      [
        'test,dt.entity.host="HOST-2345EFA5657",type="ABC" 1',
        ['test', ['type' => 'ABC', 'dt.entity.host' => "HOST-WILL-CHANGE"]],
        'HOST-2345EFA5657',
      ],
      [
        'test,type_value="ABC",label_value="4321" 1',
        ['test', ['TypeValue' => 'ABC', 'labelValue' => '4321']],
      ],
    ];
  }

  public function testInvalidDimensions(): void {
    $this->expectError();
    new Metric('test', [1, 2]);
  }

}

/**
 * Concrete class for testing.
 */
class Metric extends MetricBase {

  /**
   * {@inheritdoc}
   */
  protected function payloadToString(): string {
    return '1';
  }

}
