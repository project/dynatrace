<?php

namespace Drupal\Tests\dynatrace\Unit;

use Drupal\dynatrace\Api\Count;
use Drupal\dynatrace\Api\IngestMetric;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;
use Psr\Log\LoggerInterface;
use Psr\Log\LoggerTrait;

/**
 * @coversDefaultClass \Drupal\dynatrace\Api\IngestMetric
 *
 * @group dynatrace
 */
class IngestMetricTest extends UnitTestCase implements LoggerInterface {
  use LoggerTrait;

  private array $logs = [];

  private array $historyContainer = [];

  public function setUpIngestMetric(Response $response, string $base_url = 'http://example.com', string $host = '', string $api_key = 'METRIC_API_KEY'): IngestMetric {
    $history = Middleware::history($this->historyContainer);
    $mock = new MockHandler([
      $response,
    ]);
    $handlerStack = HandlerStack::create($mock);
    $handlerStack->push($history);
    $client = new Client(['handler' => $handlerStack]);
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $this->getConfigFactoryStub([
      'dynatrace.settings' => [
        'base_url' => $base_url,
        'host' => $host,
        'api_keys' => [
          'metrics' => $api_key,
        ],
      ],
    ]);
    return new IngestMetric($client, $config_factory, $this);
  }

  public function testPostMetric(): void {
    $ingest_metric = $this->setupIngestMetric(new Response(202, ['Content-Length' => '0']));
    $count = new Count('example', ['dimension' => 'value']);

    $this->assertTrue($ingest_metric->postMetric($count));
    /** @var \GuzzleHttp\Psr7\Request $request */
    $request = $this->historyContainer[0]['request'];
    $this->assertSame('example,dimension="value" count,delta=1', (string) $request->getBody());
    $this->assertSame('Api-Token METRIC_API_KEY', $request->getHeader('Authorization')[0]);
    $this->assertSame('http://example.com/metrics/ingest', (string) $request->getUri());
    $this->assertEmpty($this->logs);
  }

  public function testPostMetricWithHost(): void {
    $ingest_metric = $this->setupIngestMetric(new Response(202, ['Content-Length' => '0']), host: 'HOST-TEST');
    $count = new Count('example', ['dimension' => 'value']);

    $this->assertTrue($ingest_metric->postMetric($count));
    /** @var \GuzzleHttp\Psr7\Request $request */
    $request = $this->historyContainer[0]['request'];
    $this->assertSame('example,dt.entity.host="HOST-TEST",dimension="value" count,delta=1', (string) $request->getBody());
    $this->assertSame('Api-Token METRIC_API_KEY', $request->getHeader('Authorization')[0]);
    $this->assertSame('http://example.com/metrics/ingest', (string) $request->getUri());
    $this->assertEmpty($this->logs);
  }

  public function testPostMetric404(): void {
    $ingest_metric = $this->setupIngestMetric(new Response(400, ['Content-Length' => '0']), host: 'HOST-TEST');
    $count = new Count('example', ['dimension' => 'value']);

    $this->assertFalse($ingest_metric->postMetric($count));
    /** @var \GuzzleHttp\Psr7\Request $request */
    $request = $this->historyContainer[0]['request'];
    $this->assertSame('example,dt.entity.host="HOST-TEST",dimension="value" count,delta=1', (string) $request->getBody());
    $this->assertSame('Api-Token METRIC_API_KEY', $request->getHeader('Authorization')[0]);
    $this->assertSame('http://example.com/metrics/ingest', (string) $request->getUri());
    $this->assertCount(1, $this->logs);
    $this->assertSame('error', $this->logs[0]['level']);
    $this->assertSame('Unable to post %body to @uri. Exception: %type: @message in %function (line %line of %file).', $this->logs[0]['message']);
    $this->assertSame('example,dt.entity.host="HOST-TEST",dimension="value" count,delta=1', $this->logs[0]['context']['%body']);
    $this->assertSame('http://example.com/metrics/ingest', $this->logs[0]['context']['@uri']);
    $this->assertArrayHasKey('@backtrace_string', $this->logs[0]['context']);
  }

  public function log($level, $message, array $context = []): void {
    $this->logs[] = [
      'level' => $level,
      'message' => $message,
      'context' => $context,
    ];
  }

  /**
   * @covers ::isConfigured
   * @dataProvider providerIsConfigured
   */
  public function testIsConfigured(string $base_url, string $host, string $api_key, bool $expected): void {
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $this->getConfigFactoryStub([
      'dynatrace.settings' => [
        'base_url' => $base_url,
        'host' => $host,
        'api_keys' => [
          'metrics' => $api_key,
        ],
      ],
    ]);
    $ingest_metric = new IngestMetric($this->prophesize(Client::class)->reveal(), $config_factory, $this);
    $this->assertSame($expected, $ingest_metric->isConfigured());
  }

  public static function providerIsConfigured(): array {
    return [
      ['', '', '', FALSE],
      ['http://example.com', '', '', FALSE],
      ['http://example.com', 'HOST-ID', '', FALSE],
      ['http://example.com', 'HOST-ID', '1324321432', TRUE],
      ['http://example.com', '', '1324321432', TRUE],
    ];
  }

}
