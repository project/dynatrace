<?php

namespace Drupal\Tests\dynatrace\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the Dynatrace module.
 *
 * @group dynatrace
 */
class DynatraceTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['dynatrace'];

  /**
   * Tests the normal operation of the module.
   */
  public function testDynatraceHappyPath(): void {
    // Login as an admin user to set up VG Wort, use the field UI and create
    // content.
    $admin_user = $this->createUser([], 'site admin', TRUE);
    $this->drupalLogin($admin_user);

    // Module settings.
    $this->drupalGet('admin/config');
    $this->clickLink('Dynatrace');
    $this->assertSession()->fieldExists('base_url')->setValue('http://example.com/');
    $this->assertSession()->fieldExists('host')->setValue('HOST-AEF12343534');
    $this->assertSession()->fieldExists('api_keys[metrics]')->setValue('QWERTY');
    $this->submitForm([], 'Save configuration');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $config = $this->config('dynatrace.settings');
    // Right hand should be trimmed.
    $this->assertSession()->fieldValueEquals('base_url', 'http://example.com');
    $this->assertSame('http://example.com', $config->get('base_url'));
    $this->assertSame('HOST-AEF12343534', $config->get('host'));
    $this->assertSame('QWERTY', $config->get('api_keys.metrics'));

    // Ensure the module can be uninstalled without everything breaking.
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/modules/uninstall');
    $this->assertSession()->fieldExists('uninstall[dynatrace]')->check();
    $this->submitForm([], 'Uninstall');
    $this->submitForm([], 'Uninstall');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->fieldNotExists('uninstall[dynatrace]');
    $this->drupalLogout();
    $this->assertSession()->statusCodeEquals(200);
  }

}
