<?php

namespace Drupal\dynatrace;

use OneAgent\OneAgentSDK;

/**
 * A wrapper to get the OneAgent instance if the extension is installed.
 */
class OneAgent {

  /**
   * The OneAgent instance.
   */
  protected ?OneAgentSDK $instance = NULL;

  /**
   * Gets the OneAgent instance if available.
   *
   * @return \OneAgent\OneAgentSDK|null
   *   The OneAgent instance if available.
   */
  public function getInstance(): ?OneAgentSDK {
    if (!isset($this->instance) && class_exists('OneAgent\OneAgentSDK')) {
      $this->instance = new OneAgentSDK();
    }
    return $this->instance;
  }

}
