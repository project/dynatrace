<?php

namespace Drupal\dynatrace\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\dynatrace\OneAgent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for Dynatrace paths.
 */
class DynatraceController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Constructs the BlockListController.
   *
   * @param \Drupal\dynatrace\OneAgent $oneAgent
   *   The one agent wrapper.
   */
  public function __construct(protected OneAgent $oneAgent) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('dynatrace.one_agent')
    );
  }

  /**
   * The trace information.
   *
   * @return array
   *   A render array of OneAgent trace info.
   */
  public function getTraceInfo(): array {
    $output = [];
    $one_agent = $this->oneAgent->getInstance();
    if (!$one_agent) {
      $output['info'] = [
        '#markup' => $this->t('The Dynatrace extension is not configured'),
      ];
      return $output;
    }

    $output['info'] = [
      '#type' => 'details',
      '#title' => $this->t('Trace info'),
      '#description' => $this->t('One Agent trace information'),
      '#open' => TRUE,
    ];
    $output['info']['context_info'] = [
      '#type' => 'item',
      '#title' => $this->t('Context Information'),
      '#markup' => $one_agent->getTraceContextInfo(),
    ];
    $output['info']['trace_id'] = [
      '#type' => 'item',
      '#title' => $this->t('Trace ID'),
      '#markup' => $one_agent->getTraceId(),
    ];
    $output['info']['span_id'] = [
      '#type' => 'item',
      '#title' => $this->t('Span ID'),
      '#markup' => $one_agent->spanId(),
    ];
    $output['info']['is_valid'] = [
      '#type' => 'item',
      '#title' => $this->t('Is valid?'),
      '#markup' => (bool) $one_agent->isValid(),
    ];

    return $output;
  }

}
