<?php

namespace Drupal\dynatrace\Api;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Utility\Error;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;

/**
 * Posts custom metrics to Dynatrace.
 *
 * @see https://www.dynatrace.com/support/help/extend-dynatrace/extend-metrics/reference/metric-ingestion-protocol
 * @see https://www.dynatrace.com/support/help/dynatrace-api/environment-api/metric-v2/post-ingest-metrics
 */
class IngestMetric {

  /**
   * The default message for logging errors.
   */
  private const ERROR_MESSAGE = 'Unable to post %body to @uri.';

  /**
   * The path to post custom metrics to.
   */
  private const API_ENDPOINT = '/metrics/ingest';

  /**
   * The dynatrace.settings configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected readonly ImmutableConfig $config;

  /**
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP client.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The Dynatrace logger channel.
   */
  public function __construct(protected readonly ClientInterface $httpClient, ConfigFactoryInterface $configFactory, protected readonly LoggerInterface $logger) {
    $this->config = $configFactory->get('dynatrace.settings');
  }

  /**
   * Determines if the ingest metric configuration is present.
   *
   * @return bool
   *   TRUE if the ingest metric configuration is present, FALSE if not.
   */
  public function isConfigured() : bool {
    return strlen($this->config->get('base_url')) > 0 && strlen($this->config->get('api_keys.metrics')) > 0;
  }

  /**
   * Posts custom metrics to Dynatrace.
   *
   * @param \Drupal\dynatrace\Api\MetricBase $metric
   *   The metric to post to Dynatrace.
   *
   * @return bool
   *   TRUE if the metric is successfully posted to Dynatrace, FALSE if not.
   *   Errors are logged to the Dynatrace logger channel.
   */
  public function postMetric(MetricBase $metric): bool {
    $host = $this->config->get('host');
    if (strlen($host) > 0) {
      $metric->setHost($host);
    }
    $options = [
      'body' => (string) $metric,
      'headers' => [
        'Authorization' => 'Api-Token ' . $this->config->get('api_keys.metrics'),
        'Content-Type' => 'text/plain',
        'Accept' => 'application/json; charset=utf-8',
      ],
      // Throw exceptions for HTTP errors.
      'http_errors' => TRUE,
    ];
    $uri = $this->config->get('base_url') . self::API_ENDPOINT;

    try {
      $this->httpClient->request('post', $uri, $options);
    }
    catch (GuzzleException $e) {
      $error = Error::decodeException($e) + [
        '%body' => $options['body'],
        '@uri' => $uri,
      ];
      $this->logger->error(self::ERROR_MESSAGE . ' Exception: ' . Error::DEFAULT_ERROR_MESSAGE, $error);
      return FALSE;
    }
    return TRUE;
  }

}
