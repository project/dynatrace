<?php

namespace Drupal\dynatrace\Api;

/**
 * Data points of the count type are deltas between the previous and current
 * data points.
 *
 * For example, if the initial data point has the value of 500 and the second
 * data point has the value of 1,000, the actual stored value at the timestamp
 * of the second data point is 1,500.
 *
 * @see https://www.dynatrace.com/support/help/extend-dynatrace/extend-metrics/reference/metric-ingestion-protocol
 */
class Count extends MetricBase {

  /**
   * The delta to send to Dynatrace.
   *
   * @var int
   */
  private int $delta = 1;

  /**
   * Sets the count delta.
   *
   * @param int $delta
   *   A numeric value.
   *
   * @return $this
   */
  public function setDelta(int $delta): static {
    $this->delta = $delta;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function payloadToString(): string {
    return 'count,delta=' . $this->delta;
  }

}
