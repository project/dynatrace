<?php

namespace Drupal\dynatrace\Api;

use Drupal\Component\Assertion\Inspector;

/**
 * @see https://www.dynatrace.com/support/help/extend-dynatrace/extend-metrics/reference/metric-ingestion-protocol
 */
abstract class MetricBase {

  /**
   * A Dynatrace reserved dimension key that relates the metric to host.
   */
  private const HOST_DIMENSION = 'dt.entity.host';

  /**
   * @param string $metricKey
   *   The key of the metric you're submitting. It consists of sections,
   *   separated by dots, for example first.second.third.
   *   Allowed characters are lowercase and uppercase letters, numbers,
   *   hyphens (-), and underscores (_). The following restrictions apply:
   *   - Non-latin letters (like ö) are not allowed.
   *   - Metric keys cannot start with a number or a hyphen (-).
   *   - Sections cannot start with a hyphen (-).
   *   - The length of the key must be in range from 3 to 250 characters.
   * @param string[] $dimensions
   *   (optional) Key value pairs. You can specify up to 50 dimensions. If you
   *   ingest a dimension with an empty value, the whole dimension tuple is
   *   dropped at ingestion time. For instance, if you ingest dimEmpty="", the
   *   dimension dimEmpty is removed.
   * @param int|null $timestamp
   *   (optional) The format of the timestamp is UTC milliseconds. Allowed range
   *   is between 1 hour into the past and 10 minutes into the future from now.
   *   If no timestamp is provided, the current timestamp of the server is used.
   */
  public function __construct(private readonly string $metricKey, private array $dimensions = [], private readonly ?int $timestamp = NULL) {
    assert(Inspector::assertAllStrings($this->dimensions));
    assert(Inspector::assertAllStrings(array_keys($this->dimensions)));
    // @todo decide if we validate anything.
  }

  /**
   * Sets the Dynatrace reserved dimension key for HOST.
   *
   * Note: You don't need to specify the dt.entity.host dimension when using
   * local ingestion methods via OneAgent, that is dynatrace_ingest and local
   * API, because for this ingestion methods, the host context is added
   * automatically.
   *
   * @param string $host
   *   The Dynatrace host name.
   *
   * @return $this
   */
  public function setHost(string $host) {
    $this->dimensions = [self::HOST_DIMENSION => $host] + $this->dimensions;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function __toString(): string {
    $output = $this->metricKey;
    foreach ($this->dimensions as $key => $value) {
      // Make sure the keys are Snake case.
      $key = strtolower(preg_replace('/[A-Z]/', '_\\0', lcfirst($key)));
      $output .= ",$key=\"$value\"";
    }
    $output .= ' ' . $this->payloadToString();
    if (is_int($this->timestamp)) {
      $output .= ' ' . $this->timestamp;
    }
    return $output;
  }

  /**
   * Converts a payload to a string to send to Dynatrace.
   *
   * @return string
   *   The payload as a string.
   */
  abstract protected function payloadToString(): string;

}
