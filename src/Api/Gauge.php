<?php

namespace Drupal\dynatrace\Api;

/**
 * @see https://www.dynatrace.com/support/help/extend-dynatrace/extend-metrics/reference/metric-ingestion-protocol
 */
class Gauge extends MetricBase {

  /**
   * An array of numeric values.
   *
   * @var int[]|float[]
   */
  private $values = [];

  /**
   * Adds a value to gauge.
   *
   * @param int|float $value
   *   A numeric value.
   *
   * @return $this
   */
  public function addValue(int|float $value): static {
    $this->values[] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function payloadToString(): string {
    $output = 'gauge,';
    $count = count($this->values);
    if ($count < 2) {
      $output .= $this->values[0] ?? 1;
    }
    else {
      foreach ($this->values as $value) {
        $min = isset($min) ? min($value, $min) : $value;
        $max = isset($max) ? max($value, $max) : $value;
        $sum = isset($sum) ? $sum + $value : $value;
      }
      // These variable will always be set be stops PHPStan, PHPCS and IDEs from
      // complaining.
      if (isset($min) && isset($max) && isset($sum)) {
        $output .= "min=$min,max=$max,sum=$sum,count=$count";
      }
    }
    return $output;
  }

}
