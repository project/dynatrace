<?php

namespace Drupal\dynatrace\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a settings form to configure the Dynatrace module.
 */
class DynatraceSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'dynatrace_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['dynatrace.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = [];

    $form['base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Base URL'),
      '#description' => $this->t('Enter the base URL to post Dynatrace metrics to. Example URIs: <br/><ul><li>Managed: https://{your-domain}/e/{your-environment-id}/api/v2</li><li>SaaS: https://{your-environment-id}.live.dynatrace.com/api/v2</li><li>Environment ActiveGate: https://{your-activegate-domain}/e/{your-environment-id}/api/v2</li></ul>'),
      '#default_value' => $this->config('dynatrace.settings')->get('base_url'),
    ];

    // @todo https://www.dynatrace.com/support/help/dynatrace-api/environment-api/topology-and-smartscape/hosts-api/get-all
    //   Validate this using the API.
    $form['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Host'),
      '#description' => $this->t('Map the data points to this host ID. This host ID must exist or data will not be excepted by Dynatrace. You don\'t need to specify the host when using local ingestion methods via OneAgent, for example <a target="_blank" href="https://www.dynatrace.com/support/help/extend-dynatrace/extend-metrics/ingestion-methods/oneagent-metric-api">local API</a>, because for this ingestion methods, the host context is added automatically.'),
      '#default_value' => $this->config('dynatrace.settings')->get('host'),
    ];

    $form['api_keys'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];
    $form['api_keys']['metrics'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Metrics API Key'),
      '#description' => $this->t('Enter a Dynatrace API key that can write metrics.'),
      '#default_value' => $this->config('dynatrace.settings')->get('api_keys.metrics'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('dynatrace.settings')
      ->set('base_url', rtrim($form_state->getValue('base_url'), '/'))
      ->set('host', $form_state->getValue('host'))
      ->set('api_keys.metrics', $form_state->getValue(['api_keys', 'metrics']))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
